package macaroon_security

import (
	b64 "encoding/base64"
	qt "github.com/frankban/quicktest"
	"testing"
)


func TestCreateMacaroon(test *testing.T) {
	securityMacaroon := Initialize("mongo:27017")
	c := qt.New(test)

	resources := [] ResourceRequest{
		{
			Action:   "read",
			File:     "fileTest",
			Duration: "",
				},
	}
	requestValidator := MacaroonRequest{
		Sender:   "test1",
		Ressources: resources,
	}

	condition := "test1" + "," + "read" + "," + "fileTest"
	resultMacaroon, err := securityMacaroon.CreateRequest(requestValidator)
	c.Assert(err, qt.IsNil)
	caveats := resultMacaroon.M().Caveats()
	id := string(caveats[0].Id)
	//c.Assert(errConversion, qt.IsNil)
	c.Assert(id, qt.Equals, condition)

}

func TestReadMacaroon(test *testing.T) {
	securityMacaroon := Initialize("mongo:27017")
	c := qt.New(test)
	resources := [] ResourceRequest{
		{
			Action:   "read",
			File:     "fileTest",
			Duration: "",
		},
	}

	requestValidator := MacaroonRequest{
		Sender:   "test2",
		Ressources: resources,
	}

	resultMacaroon, err := securityMacaroon.CreateRequest(requestValidator)
	if err != nil {
		c.Error(err)
		c.Fail()
	}

	macaroonByte, err := resultMacaroon.MarshalJSON()
	if err != nil {
		c.Error(err)
		c.Fail()
	}

	macaroonEncode := b64.StdEncoding.EncodeToString(macaroonByte)
	operation, conditionResult, err := securityMacaroon.ReadResource([]byte(macaroonEncode))
	if err != nil {
		c.Error(err)
		c.Fail()
	}

	c.Assert(err, qt.IsNil)
	c.Assert(len(conditionResult), qt.Equals, 1)
	c.Assert(len(operation), qt.Equals, 1)
}

func TestModifyMacaroon(test *testing.T) {
	securityMacaroon := Initialize("mongo:27017")
	c := qt.New(test)
	resources := [] ResourceRequest{
		{
			Action:   "read",
			File:     "fileTest",
			Duration: "",
		},
	}

	requestValidator := MacaroonRequest{
		Sender:   "test3",
		Ressources: resources,
	}
	macaroon, errCreation := securityMacaroon.CreateRequest(requestValidator)
	macaroonByte, errConvJson := macaroon.MarshalJSON()
	println(macaroonByte)
	c.Assert(errCreation, qt.IsNil)
	c.Assert(errConvJson, qt.IsNil)
	macaroonModify := []byte(`{"Macaroon" : "eyJjIjpbeyJpIjoiY2x1c3RlcjIscmVhZCx0ZXN0NC9maWxlLnR4dCJ9XSwibCI6ImxvY2FsIiwiaTY0IjoiQXdvUTRVSC1zYW0yYkRGdDVzQ2JGRTZadEJJZ00yVmpNalV6TTJRek1qWmlOV05rT0RrNU1qSXhOMlkwT1RjM05tSXlPRGthRUFvSVkyeDFjM1JsY2pJU0JISmxZV1EiLCJzNjQiOiJTb1pvVEpXM0dyUUlOU0ZoRnA5UVdfVlBhUEhRVm1LYTNZSWFXWndnTVFvIn0=" }`)
	response, errModification := securityMacaroon.ModifyMaroon(macaroonModify)
	c.Assert(errModification, qt.IsNil)
	c.Assert(len(response), qt.Not(qt.Equals), 0)
}

func TestValidityCaveat(test *testing.T) {
	securityMacaroon := Initialize("mongo:27017")
	c := qt.New(test)
	resources := [] ResourceRequest{
		{
		Action:   "read",
		File:     "fileTest",
		Duration: "",
		},
	}
	requestValidator := MacaroonRequest{
		Sender:   "test2",
		Ressources: resources,
	}

	resultMacaroon, err := securityMacaroon.CreateRequest(requestValidator)
	result := ValidationCaveat(resultMacaroon, "test2")
	c.Assert(err, qt.IsNil)
	c.Assert(result, qt.Equals, true)
}

func TestValidityDate(test *testing.T) {
	securityMacaroon := Initialize("mongo:27017")
	c := qt.New(test)
	resources := []ResourceRequest{
		{
			Action:   "read",
			File:     "fileTest",
			Duration: "10m",
		},
	}
	requestValidator := MacaroonRequest{
		Sender:   "test2",
		Ressources: resources,
	}
	resultMacaroon, err := securityMacaroon.CreateRequest(requestValidator)
	c.Assert(err, qt.IsNil)
	testDate := string(resultMacaroon.M().Caveats()[0].Id)
	result := ValidationCaveatDate(testDate)
	c.Assert(result, qt.Equals, true)
}

func TestCreateMacaroonManyResources(test *testing.T) {
	securityMacaroon := Initialize("mongo:27017")
	c := qt.New(test)
	resources := []ResourceRequest{
		{
		Action:   "read",
		File:     "fileTest",
		Duration: "",
	},
	{
		Action:   "write",
		File:     "fileTest2",
		Duration: "",
	},
	}

	requestValidator := MacaroonRequest{
		Sender:   "test1",
		Ressources: resources,
	}

	condition := requestValidator.Sender + "," + resources[0].Action + "," + resources[0].File
	condition2 := requestValidator.Sender + "," + resources[1].Action + "," + resources[1].File
	resultMacaroon, err := securityMacaroon.CreateRequest(requestValidator)
	c.Assert(err, qt.IsNil)
	caveats := resultMacaroon.M().Caveats()
	id := string(caveats[0].Id)
	id2 :=string(caveats[1].Id)
	//c.Assert(errConversion, qt.IsNil)
	c.Assert(id, qt.Equals, condition)
	c.Assert(id2, qt.Equals, condition2)

}
