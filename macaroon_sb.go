package macaroon_security

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"github.com/juju/mgo/v2"
	"gopkg.in/macaroon-bakery.v3/bakery"
	"gopkg.in/macaroon-bakery.v3/bakery/checkers"
	"gopkg.in/macaroon-bakery.v3/bakery/mgorootkeystore"
	"gopkg.in/macaroon-bakery.v3/httpbakery"
	"gopkg.in/macaroon.v2"
)

const (
	AuthDatabase    = "local"
	KeyPairFileName = "keyMacaroon.json"
	layoutISO       = "2006-01-02T15:04:05-0700"
)

// CreateRequest create a Macaroon from the given MacaroonRequest
func (mss *MacaroonSecurityService) CreateRequest(requestValidator MacaroonRequest) (*bakery.Macaroon, error) {

	var caveats []checkers.Caveat
	var operation bakery.Op
	for _, caveatVar := range requestValidator.Ressources {
		var condition = ""
		if len(caveatVar.Duration) != 0 {
			durationTime, err := time.ParseDuration(caveatVar.Duration)
			if err != nil {
				return nil, fmt.Errorf("format duration not compatible")
			}
			dateEnd := time.Now().UTC().Add(durationTime)

			condition = requestValidator.Sender + "," + caveatVar.Action + "," + caveatVar.File + "," + "date end" + "," + dateEnd.Format(layoutISO)
		} else {
			condition = requestValidator.Sender + "," + caveatVar.Action + "," + caveatVar.File
		}
		caveats = append(caveats, checkers.Caveat{
			Condition: condition,
			Namespace: "",
			Location:  "",
		})
		operation = bakery.Op{Entity: requestValidator.Sender, Action: caveatVar.Action}
	}
	return mss.Oven.NewMacaroon(context.TODO(), bakery.Version2, caveats, operation)
}

// ReadResource parse the Json Base64 encoded, check the Macaroon, and return the Op inside
func (mss *MacaroonSecurityService) ReadResource(jsonData []byte) ([]bakery.Op, []string, error) {
	decode, err := base64.StdEncoding.DecodeString(string(jsonData))
	if err != nil {
		return nil, nil, err
	}

	var mac macaroon.Macaroon
	if err := mac.UnmarshalJSON(decode); err != nil {
		return nil, nil, err
	}

	var slice = make(macaroon.Slice, 1)
	slice[0] = &mac
	return mss.Oven.VerifyMacaroon(context.TODO(), slice)
}

func (mss *MacaroonSecurityService) ModifyMaroon(jsonData []byte) ([]byte, error) {
	request := MacaroonCaveatRequest{}
	if err := json.Unmarshal(jsonData, &request); err != nil {
		return nil, err
	}

	decode, err := base64.StdEncoding.DecodeString(request.Macaroon)
	if err != nil {
		return nil, err
	}

	var mac macaroon.Macaroon
	if err = mac.UnmarshalJSON(decode); err != nil {
		return nil, err
	}

	caveats := checkers.Caveat{
		Condition: request.Caveat,
		Location:  "",
	}

	var macaroonTmp *bakery.Macaroon
	macaroonTmp, _ = bakery.NewLegacyMacaroon(&mac)
	if err = mss.Oven.AddCaveat(context.TODO(), macaroonTmp, caveats); err != nil {
		return nil, err
	}

	response, err := macaroonTmp.MarshalJSON()
	if err != nil {
		return nil, err
	}
	return response, nil
}

func ValidationCaveat(macaroon *bakery.Macaroon, validation string) bool {
	caveats := macaroon.M().Caveats()
	for _, cav := range caveats {
		cavString := string([]byte(cav.Id))
		if len(cavString) >= len(validation) && (strings.Contains(cavString, validation)) {
			return ValidationCaveatDate(cavString)
		}
	}
	return false
}

func ValidationCaveatDate(cavString string) bool {

	if strings.Contains(cavString, "date end") {
		currenTimeVar := time.Now().UTC()
		variables := strings.Split(string(cavString), ",")
		date := variables[len(variables)-1]
		dateTime, err := time.Parse(layoutISO, date)
		if err != nil {
			return false
		}
		return currenTimeVar.Before(dateTime)
	}
	return true
}

func Initialize(mongoURI string) IMacaroonSecurity {

	rootKeys := mgorootkeystore.NewRootKeys(50)

	dateNow := time.Now().UTC()
	dateExpire := time.Now().AddDate(5, 0, 0)
	duration := dateExpire.Sub(dateNow)

	policy := mgorootkeystore.Policy{
		GenerateInterval: 0,
		ExpiryDuration:   duration,
	}
	dialInfo, err := mgo.ParseURL(mongoURI)
	if err != nil {
		panic(err)
	}
	// set tmeout
	dialInfo.Timeout = 60 * time.Second
	// set database name
	if dialInfo.Database == "" {
		dialInfo.Database = AuthDatabase
	}

	log.Println("Trying to connect mongo db", dialInfo.Addrs, dialInfo.Database)
	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		log.Panicln("Unable to connect to mongo:", err.Error())
	}

	database := mgo.Database{
		Name:    "local",
		Session: session,
	}

	checkCollectionEmpty(database)

	// store keyPair
	log.Println("Create KeyPair")
	keyPair := initKeyPair()
	log.Println(keyPair)

	collection := mgo.Collection{
		Name:     "keymacaroon",
		FullName: "local.keymacaroon",
		Database: &database,
	}
	keystore := rootKeys.NewStore(&collection, policy)

	log.Println()

	prefix := make(map[string]string)
	prefix["p1"] = "firstNameSpace"
	namespace := checkers.NewNamespace(prefix)
	thirdPartyLocator := httpbakery.NewThirdPartyLocator(nil, nil)

	ovenParams := bakery.OvenParams{
		Key:              keyPair,
		Namespace:        namespace,
		Location:         "local",
		Locator:          thirdPartyLocator,
		LegacyMacaroonOp: bakery.NoOp,
		RootKeyStoreForOps: func(ops []bakery.Op) bakery.RootKeyStore {
			return keystore
		},
	}
	oven := bakery.NewOven(ovenParams)
	return &MacaroonSecurityService{Oven: oven, KeyPair: keyPair}
}

func checkCollectionEmpty(database mgo.Database) {
	names, err := database.CollectionNames()
	if err != nil {
		log.Printf("failed to gel coll names: %v", err)
		return
	}

	for _, name := range names {
		if name == "keymacaroon" {
			log.Printf("The collection exist!")
			break
		}
	}
}

// initKeyPair initialize a keyPair from the "keyMacaroon.json" file, or create one if not exists.
func initKeyPair() *bakery.KeyPair {
	var keyPair *bakery.KeyPair

	if _, err := os.Stat(KeyPairFileName); err != nil {
		log.Println("keyfile not found, create key")
		keyPair, _ := createKeyPair()
		return keyPair
	}

	jsonFile, err := os.Open(KeyPairFileName)
	defer jsonFile.Close()
	if err != nil {
		log.Panicln("keyfile found, but unable to open it")
		return nil
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	if err := json.Unmarshal(byteValue, &keyPair); err != nil {
		log.Panicln("Unable to unmarshal keypair")
		return nil
	}

	return keyPair
}

// createKeyPair create a KeyPair and save it into keyMacaroon.json
func createKeyPair() (*bakery.KeyPair, error) {
	keyPair, err := bakery.GenerateKey()
	if err != nil {
		return nil, err
	}

	file, err := json.MarshalIndent(keyPair, "", " ")
	if err != nil {
		return nil, err
	}

	if err = ioutil.WriteFile(KeyPairFileName, file, 0644); err != nil {
		return nil, err
	}

	return keyPair, nil
}
