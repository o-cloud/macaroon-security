module gitlab.com/o-cloud/macaroon-security

go 1.15

require (
	github.com/frankban/quicktest v1.11.3
	github.com/golang/protobuf v1.5.1 // indirect
	github.com/juju/mgo/v2 v2.0.0-20210302023703-70d5d206e208
	github.com/patrickmn/go-cache v2.1.0+incompatible
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	golang.org/x/sys v0.0.0-20210330210617-4fbd30eecc44 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/macaroon-bakery.v3 v3.0.0
	gopkg.in/macaroon.v2 v2.1.0
)
