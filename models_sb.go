package macaroon_security

import "gopkg.in/macaroon-bakery.v3/bakery"

type MacaroonRequest struct {
	Sender   string
	Ressources [] ResourceRequest
}

type MacaroonCaveatRequest struct {
	Macaroon string `json:"macaroon"`
	Caveat   string `json:"caveat"`
}

type ResourceRequest struct {
	Action   string
	File     string
	Duration string
}

type MacaroonSecurityService struct {
 	Oven *bakery.Oven
 	KeyPair *bakery.KeyPair
}

type IMacaroonSecurity interface {
 CreateRequest(requestValidator MacaroonRequest) (*bakery.Macaroon, error)
 ReadResource(jsonData []byte) ([]bakery.Op, []string, error)
 ModifyMaroon(jsonData []byte) ([]byte, error)
}